#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

struct data
{
    int mulaiHitung;
    int akhirHitung;
};

void* hitung(void *arg)
{
    struct data *pilih = arg;
    int hasil=0;
    for(int i=pilih->mulaiHitung;i<=pilih->akhirHitung;i++)
    {
        hasil+=i;
    }
}

int main()
{
    struct data pilih;
    int n,i,hasil=0;
	clock_t waktuAwal, waktuAkhir;
	double selangWaktu;
    pthread_t thread[4];
    printf("Masukkan n: ");
	scanf("%d", &n);
	//default
	waktuAwal=clock();
	for(i=1;i<=n;i++)
	{
		hasil+=i;
	}
    waktuAkhir=clock();
	selangWaktu=(double)(waktuAkhir-waktuAwal)/CLOCKS_PER_SEC;
	printf("Default: %f\n", selangWaktu);
	//1 thread
	pilih.mulaiHitung=1;
	pilih.akhirHitung=n;
	waktuAwal=clock();
    pthread_create(&thread[0],NULL,hitung,(void *)&pilih);
    pthread_join(thread[0],NULL);
    waktuAkhir=clock();
	selangWaktu=(double)(waktuAkhir-waktuAwal)/CLOCKS_PER_SEC;
	printf("1 Thread: %f\n", selangWaktu);
	//2 thread
	waktuAwal=clock();
	for(i=0;i<2;i++)
    {
        if(i==0)
        {
            pilih.mulaiHitung=1;
            pilih.akhirHitung=n/2;
        }
        else if(i==1)
        {
            pilih.mulaiHitung=n/2+1;
            pilih.akhirHitung=n;
        }
        pthread_create(&thread[i],NULL,hitung,(void *)&pilih);
    }
    for(i=0;i<2;i++)
    {
        pthread_join(thread[i],NULL);
    }
    waktuAkhir=clock();
	selangWaktu=(double)(waktuAkhir-waktuAwal)/CLOCKS_PER_SEC;
	printf("2 Thread: %f\n", selangWaktu);
    //3 thread
    for(i=0;i<3;i++)
    {
        if(i==0)
        {
            pilih.mulaiHitung=1;
            pilih.akhirHitung=n/3;
        }
        else if(i==1)
        {
            pilih.mulaiHitung=n/3+1;
            pilih.akhirHitung=n/3*2;
        }
        else if(i==2)
        {
            pilih.mulaiHitung=n/3*2+1;
            pilih.akhirHitung=n;
        }
        pthread_create(&thread[i],NULL,hitung,(void *)&pilih);
    }
    for(i=0;i<3;i++)
    {
        pthread_join(thread[i],NULL);
    }
    waktuAkhir=clock();
	selangWaktu=(double)(waktuAkhir-waktuAwal)/CLOCKS_PER_SEC;
	printf("3 Thread: %f\n", selangWaktu);
    //4 thread
    for(i=0;i<4;i++)
    {
        if(i==0)
        {
            pilih.mulaiHitung=1;
            pilih.akhirHitung=n/4;
        }
        else if(i==1)
        {
            pilih.mulaiHitung=n/4+1;
            pilih.akhirHitung=n/4*2;
        }
        else if(i==2)
        {
            pilih.mulaiHitung=n/4*2+1;
            pilih.akhirHitung=n/4*3;
        }
        else if(i==3)
        {
            pilih.mulaiHitung=n/4*3+1;
            pilih.akhirHitung=n;
        }
        pthread_create(&thread[i],NULL,hitung,(void *)&pilih);
    }
    for(i=0;i<4;i++)
    {
        pthread_join(thread[i],NULL);
    }
    waktuAkhir=clock();
	selangWaktu=(double)(waktuAkhir-waktuAwal)/CLOCKS_PER_SEC;
	printf("4 Thread: %f\n", selangWaktu);
    return 0;
}
