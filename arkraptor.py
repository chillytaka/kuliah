price = 0 #global variabel

#membandingkan harga tempPrice dengan global variable price
def Comparer(tempPrice):
    global price
    if (price == 0): #price == 0, berarti pertanyaan masih pertama kali jalan
        price = tempPrice
    if (tempPrice < price):
        price = tempPrice

#errm.. yaopo iki njelasno e ya... :v
def Adders(jump,target,current,tiketOrang,hargaOrang,tempPrice,total):
    if ((current+jump) < len(tiketOrang)): #cek kalo misalkan current + jump e wes lebih besar dari panjang array tiketOrang berarti skip...( menghindari out of index error )
        tempPrice = tempPrice + hargaOrang[current+jump] #menghitung total harga
        total = total + tiketOrang[current+jump] #total orang yang sudah dapet tiket masuk
        if (total < target): #kalo total orang masih kurang, nambah lagi...
            for a in range (0,target):
                Adders(a,target,current+jump,tiketOrang,hargaOrang,tempPrice,total)
    if (total == target): #cuma nge compare yang total orang e pas sesuai kebutuhan
        Comparer(tempPrice)
        return 0
    return 0 #ben recursive e mandek...

    


def main():
    kasusUji= input()
    global price #biar bisa edit global var price

    for z in range(0,kasusUji):
        tempInput = input()
        banyakPaket, banyakTanya = tempInput.split() #pisah dari tempInput jadi banyakPaket dan banyakTanya (space separator)
        #contoh tempInput = "7 3" => setelah di split() => banyakPaket = "7" dan banyakTanya = "3"
        tiketOrang = [] #jumlah orang per tiket
        hargaOrang = [] #harga tiket
        tanya = [] #jumlah pertanyaan

        #looping untuk N tiketOrang
        for a in range(0,int(banyakPaket)):
            tempOrang, tempHarga = input().split()
            tempOrang = int(tempOrang) #convert dari string ke int (hasil dari input() itu string)
            tempHarga = int(tempHarga)
            tiketOrang.append(tempOrang) #nambahi tempOrang nang array tiketOrang
            hargaOrang.append(tempHarga)  #nambahi tempHarga nang array hargaOrang

        #looping untuk M pertanyaan
        for b in range(0,int(banyakTanya)):
            tempTanya = input()
            tempTanya = int(tempTanya)
            tanya.append(tempTanya)

        tiketOrang.sort() #urutkan tiketOrang secara ascending
        hargaOrang.sort() #urutkan hargaOrang secara ascending
        #logikane, harga tiket, kalau jumlah orang nya lebih banyak harusnya harganya lebih mahal, dadi mestine bisa diurutkan..
        #diurutkan dulu ben gampang pas nyari tiketOrang maksimum

        for c in range(0,len(tanya)): #len(x) -> get panjang array x
            tanyaNow = tanya[c]

            #value reset
            get = 0
            price = 0
            counter = 0
            #end of value reset

            # mencari tiketOrang maksimum
            # anggap yang ditanyakan 3 orang, sementara jenis tiket dikasih dari 1 sampai 7 orang per tiket, 
            # kan ga perlu nyoba sampai 7 orang per tiket, cukup tiket yang dipikirkan sampe 3 orang per tiket ae...
            while(get < tanyaNow):
                if (counter != len(tiketOrang)):
                    get = tiketOrang[counter]
                    counter += 1
                    continue
                break
            #end of mencari tiketOrang maksimum

            # where the madness start.....
            Adders(0,tanyaNow,0,tiketOrang,hargaOrang,0,0)
            print(price)

            

main() #menjalankan fungsi main

#langkah :
# 1. ambil semua inputan
# 2. mengurutkan harga tiket dan jumlah orang per tiket
# 3. mencari nilai maksimum dari jumlah orang dan jenis tiket yang diperlukan
# 4. mencari kemungkinan
#    misal jumlah orang yang mau masuk = 3, pertama nyari total harga kalo 1 + 1 + 1,
#    terus nyoba lagi 1 + 2, dst....
# 5. bandingkan dengan price yang sudah di dapat
# 6. ulangi step 4 - 5 sampai angka minimum ketemu..