# Naive Bayes Style
import pandas as pd #csv operator
import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog  # qt5 things
from main import Ui_MainWindow  # ui files from qt designer

train = pd.read_csv('data.csv')  # get DATA
column = ['gender', 'StageID', 'SectionID', 'Topic', 'StudentAbsenceDays', 'raisedhands',  # List of Column Data
          'VisITedResources', 'AnnouncementsView', 'Discussion', 'Class'
          ]
data_size = train['gender'].count()  # size of data's row
result = []  # result of every iteration....

class AppWindow(QMainWindow):

# Label Probability

    # initiation
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)  # setup ui and launch

        self.ui.pushButton.clicked.connect(self.NGITUNG)

        self.show()
    
    #main program
    def NGITUNG(self):

        def Total_Probability():
            data = [0, 0, 0]
            for x in range(data_size):
                if (train[column[9]][x] == "H"):
                    data[0] += 1
                if (train[column[9]][x] == "M"):
                    data[1] += 1
                if (train[column[9]][x] == "L"):
                    data[2] += 1
            return data
        # End of Label Probability


        # Variable Probability
        def Classification_Probability(label):
            data = []
            for y in range(len(column) - 1):
                test = [0, 0, 0]
                for x in range(data_size):
                    if (y <= 4):
                        if ((train[column[y]][x] == label[y]) and (train[column[9]][x] == "H")):
                            test[0] += 1
                        if ((train[column[y]][x] == label[y]) and (train[column[9]][x] == "M")):
                            test[1] += 1
                        if ((train[column[y]][x] == label[y]) and (train[column[9]][x] == "L")):
                            test[2] += 1
                    if (y > 4 and y < len(label)):
                        if ((label[y] in range(0, 26)) and (train[column[y]][x] in range(0, 26))):
                            if (train[column[9]][x] == "H"):
                                test[0] += 1
                            if (train[column[9]][x] == "M"):
                                test[1] += 1
                            if (train[column[9]][x] == "L"):
                                test[2] += 1
                        if ((label[y] in range(26, 51)) and (train[column[y]][x] in range(26, 51))):
                            if (train[column[9]][x] == "H"):
                                test[0] += 1
                            if (train[column[9]][x] == "M"):
                                test[1] += 1
                            if (train[column[9]][x] == "L"):
                                test[2] += 1
                        if ((label[y] in range(51, 76)) and (train[column[y]][x] in range(51, 76))):
                            if (train[column[9]][x] == "H"):
                                test[0] += 1
                            if (train[column[9]][x] == "M"):
                                test[1] += 1
                            if (train[column[9]][x] == "L"):
                                test[2] += 1
                        if ((label[y] in range(76, 100)) and (train[column[y]][x] in range(76, 100))):
                            if (train[column[9]][x] == "H"):
                                test[0] += 1
                            if (train[column[9]][x] == "M"):
                                test[1] += 1
                            if (train[column[9]][x] == "L"):
                                test[2] += 1
                for a in range(3):
                    test[a] = test[a]/result[0][a]
                data.append(test)
            return data
        # End of Variable Probability


        # Print Hasil Akhir
        def Akhir(result, label):
            tempData = result[0]
            result.clear()
            result.append(tempData)
            result += Classification_Probability(label)
            tempAkhir = []
            for col in range(3):
                for row in range(len(result)):
                    if (row == 0):
                        tempResult = result[0][col]/data_size
                    else:
                        tempResult = tempResult * result[row][col]
                tempAkhir.append(tempResult)

            hasilAkhir = []
            hasil = 0
            point = 0
            for i in range(3):
                temp = tempAkhir[i]/sum(tempAkhir)
                if temp > hasil:
                    hasil = temp
                    point = i
                hasilAkhir.append(temp)

            print(hasilAkhir)
            if (point == 2):
                return "L"
            if (point == 1):
                return "M"
            if (point == 0):
                return "H"


        result.append(Total_Probability())
        label = []
        label.append(self.ui.gender.currentText()) #get from gender combobox
        label.append(self.ui.stage.currentText()) #get from stageID combobox
        label.append(self.ui.section.currentText()) #get from section combobox
        label.append(self.ui.topic.currentText()) #get from topic combobox
        label.append(self.ui.absent.currentText()) #get from absent combobox
        label.append(self.ui.raised.value()) #get from raised spinbox
        label.append(self.ui.visited.value()) #get from visited spinbox
        label.append(self.ui.announce.value()) #get from announce spinbox
        label.append(self.ui.discus.value()) #get from discus spinbox
        self.ui.result.setText(Akhir(result,label)) 


app = QApplication(sys.argv)
w = AppWindow()
w.show()
sys.exit(app.exec_())
