# Naive Bayes Style
import pandas as pd

train = pd.read_csv('ml/data.csv')
column = ['gender', 'StageID', 'SectionID', 'Topic', 'StudentAbsenceDays', 'raisedhands',
          'VisITedResources', 'AnnouncementsView', 'Discussion', 'Class'
          ]
data_size = train['gender'].count()
label = []

# Label Probability


def Total_Probability():
    data = [0, 0, 0]
    for x in range(data_size):
        if (train[column[9]][x] == "H"):
            data[0] += 1
        if (train[column[9]][x] == "M"):
            data[1] += 1
        if (train[column[9]][x] == "L"):
            data[2] += 1
    return data

# Variable Probability


def Classification_Probability(label):
    data = []
    for y in range(len(column)):
        test = [0, 0, 0]
        for x in range(data_size):
            if (y <= 5):
                if ((train[column[y]][x] == label[y]) and (train[column[9]][x] == "H")):
                    test[0] += 1
                if ((train[column[y]][x] == label[y]) and (train[column[9]][x] == "M")):
                    test[1] += 1
                if ((train[column[y]][x] == label[y]) and (train[column[9]][x] == "L")):
                    test[2] += 1
            if (y > 5):
                if (label[y] in range(0, 26)):
                    if ((train[column[y]][x] in range(0, 26)) and (train[column[9]][x] == "H")):
                        test[0] += 1
                    if ((train[column[y]][x] in range(0, 26)) and (train[column[9]][x] == "M")):
                        test[1] += 1
                    if ((train[column[y]][x] in range(0, 26)) and (train[column[9]][x] == "L")):
                        test[2] += 1
                if (label[y] in range(26, 51)):
                    if ((train[column[y]][x] in range(26, 51)) and (train[column[9]][x] == "H")):
                        test[0] += 1
                    if ((train[column[y]][x] in range(26, 51)) and (train[column[9]][x] == "M")):
                        test[1] += 1
                    if ((train[column[y]][x] in range(26, 51)) and (train[column[9]][x] == "L")):
                        test[2] += 1
                if (label[y] in range(51, 76)):
                    if ((train[column[y]][x] in range(51, 76)) and (train[column[9]][x] == "H")):
                        test[0] += 1
                    if ((train[column[y]][x] in range(51, 76)) and (train[column[9]][x] == "M")):
                        test[1] += 1
                    if ((train[column[y]][x] in range(51, 76)) and (train[column[9]][x] == "L")):
                        test[2] += 1
                if (label[y] in range(0, 26)):
                    if ((train[column[y]][x] in range(0, 26)) and (train[column[9]][x] == "H")):
                        test[0] += 1
                    if ((train[column[y]][x] in range(0, 26)) and (train[column[9]][x] == "M")):
                        test[1] += 1
                    if ((train[column[y]][x] in range(0, 26)) and (train[column[9]][x] == "L")):
                        test[2] += 1
                
        data.append(test)

    return data


label.append(Total_Probability())
label += Classification_Probability(['M', 'lowerlevel', 'A', 'IT', 'Under-7'])
print(label)
