suara3 = audioread('SARON3SL.WAV');
suara5 = audioread('SARON5SL.WAV');
suara6 = audioread('SARON6SL.WAV');
sinyal = [suara3;zeros(1000,1);suara5;zeros(1000,1);suara6;zeros(1000,1);suara3]

Fs = 48000;  % Sampling Frequency

Fstop1 = 900;             % First Stopband Frequency
Fpass1 = 950;             % First Passband Frequency
Fpass2 = 1000;             % Second Passband Frequency
Fstop2 = 1050;             % Second Stopband Frequency
Dstop1 = 0.001;           % First Stopband Attenuation
Dpass  = 0.057501127785;  % Passband Ripple
Dstop2 = 0.0001;          % Second Stopband Attenuation
flag   = 'scale';         % Sampling Flag

[N,Wn,BETA,TYPE] = kaiserord([Fstop1 Fpass1 Fpass2 Fstop2]/(Fs/2), [0 ...
                             1 0], [Dstop1 Dpass Dstop2]);

b  = fir1(N, Wn, TYPE, kaiser(N+1, BETA), flag);
Hd = dfilt.dffir(b);

y = filter(Hd,sinyal);

hsl = [];
hsl = [hsl y];

Fstop1 = 700;             % First Stopband Frequency
Fpass1 = 750;             % First Passband Frequency
Fpass2 = 800;             % Second Passband Frequency
Fstop2 = 850;             % Second Stopband Frequency

[N,Wn,BETA,TYPE] = kaiserord([Fstop1 Fpass1 Fpass2 Fstop2]/(Fs/2), [0 ...
                             1 0], [Dstop1 Dpass Dstop2]);

b  = fir1(N, Wn, TYPE, kaiser(N+1, BETA), flag);
Hd = dfilt.dffir(b);

s = filter(Hd,sinyal);
hsl=[hsl s];

Fstop1 = 800;             % First Stopband Frequency
Fpass1 = 850;             % First Passband Frequency
Fpass2 = 900;             % Second Passband Frequency
Fstop2 = 950;             % Second Stopband Frequency

[N,Wn,BETA,TYPE] = kaiserord([Fstop1 Fpass1 Fpass2 Fstop2]/(Fs/2), [0 ...
                             1 0], [Dstop1 Dpass Dstop2]);

b  = fir1(N, Wn, TYPE, kaiser(N+1, BETA), flag);
Hd = dfilt.dffir(b);
c = filter(Hd,sinyal);
hsl=[hsl c];

figure(1);
plot(hsl);
figure(2);
plot(sinyal);

% [EOF]
