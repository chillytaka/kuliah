[data1, fs1] = audioread("SARON1SL.WAV");
[data2, fs2] = audioread("SARON2SL.WAV");
[data3, fs3] = audioread("SARON3SL.WAV");
[data5, fs5] = audioread("SARON5SL.WAV");
[data6, fs6] = audioread("SARON6SL.WAV");
[data10, fs10] = audioread("saron1'sl.wav");
[data20, fs20] = audioread("saron2'sl.wav");

fft1 = fft(data1, fs1);
fft2 = fft(data2, fs2);
fft3 = fft(data3, fs3);
fft5 = fft(data5, fs5);
fft6 = fft(data6, fs6);
fft10 = fft(data10, fs10);
fft20 = fft(data20, fs20);

fft1_part = fft1(1:200);
fft2_part = fft2(1:5000);
fft3_part = fft3(1:5000);
fft5_part = fft5(1:5000);
fft6_part = fft6(1:5000);
fft10_part = fft10(1:5000);
fft20_part = fft20(1:5000);

[max_abs1, loc1] = max(abs(fft1_part));
[max_abs2, loc2] = max(abs(fft2_part));
[max_abs3, loc3] = max(abs(fft3_part));
[max_abs5, loc5] = max(abs(fft5_part));
[max_abs6, loc6] = max(abs(fft6_part));
[max_abs10, loc7] = max(abs(fft10_part));
[max_abs20, loc8] = max(abs(fft20_part));

sk = 4:4:800;
current_abs = abs(fft1_part);
plot(sk,current_abs)

sampleName = {'saron 1';'saron 2';'saron 3';'saron 5';'saron 6';'saron 1`';'saron 2`'};
sampling_rate = [fs1;fs2;fs3;fs5;fs6;fs10;fs20];
location = [loc1;loc2;loc3;loc5;loc6;loc7;loc8];
maximum = [max_abs1;max_abs2;max_abs3;max_abs5;max_abs6;max_abs10;max_abs20]
table(sampleName,sampling_rate,location,maximum)