import scipy.io.wavfile

directory = '/run/media/aufa/DataAll/Project/kuliah/search freq/SARON/'

fs1,a = scipy.io.wavfile.read(directory + 'SARON1SL.WAV')
fs2,a = scipy.io.wavfile.read(directory + 'SARON2SL.WAV')
fs3,a = scipy.io.wavfile.read(directory + 'SARON3SL.WAV')
fs5,a = scipy.io.wavfile.read(directory + 'SARON5SL.WAV')
fs6,a = scipy.io.wavfile.read(directory + 'SARON6SL.WAV')
fs10,a = scipy.io.wavfile.read(directory + "saron1'sl.wav")
fs20,a = scipy.io.wavfile.read(directory + "saron2'sl.wav")

print("Sample Name\t\t\tFrequency")
print(" ")
print("%s\t\t\t\t%i" % ("saron 1", fs1))
print("%s\t\t\t\t%i" % ("saron 2", fs2))
print("%s\t\t\t\t%i" % ("saron 3", fs3))
print("%s\t\t\t\t%i" % ("saron 5", fs5))
print("%s\t\t\t\t%i" % ("saron 6", fs6))
print("%s\t\t\t%i" % ("saron 1'", fs10))
print("%s\t\t\t%i" % ("saron 2'", fs20))