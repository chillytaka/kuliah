a = audioread('SARON5SL.WAV');

p = 8000;
h = 500;

awal = 1;
akhir = p;

start = 1;
ending = p;

hsl = [];
res = [];

for c = awal : length(a)
    if (akhir > length(a))
        break
    end
    b1 = fft(a(awal:akhir));
    c1 = abs(b1(1:500));
    hsl = [hsl c1];
    
    awal = awal + h;
    akhir = awal + p;
end

while (ending <= length(a))
   b2 = fft(a(start:ending));
   c2 = abs(b2(1:500));
   res = [res c2];
   
   start = start + p;
   ending = ending + p;
end

figure(1);
plot(hsl);

figure(2);
plot(res);

