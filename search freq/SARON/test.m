sound1 = audioread('SARON1SL.WAV');
sound2 = audioread('SARON5SL.WAV');

dist = 40000;
a = [sound1;zeros(dist*2,1)];
cut_a = a(1:300000);
b = [zeros(dist,1);sound2;zeros(500000,1)];
cut_b = b(1:300000);

total = cut_a + cut_b;

figure(2);
subplot(3,1,1);
plot(a);
subplot(3,1,2);
plot(b);
subplot(3,1,3);
plot(total);