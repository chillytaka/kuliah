import scipy.io.wavfile
import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft

directory = '/run/media/aufa/DataAll/Project/kuliah/search freq/SARON/'

fs1,a = scipy.io.wavfile.read(directory + 'SARON1SL.WAV')
# Fs
y = fft(a)
x = y [1:5000]
x0 = np.abs(x)
print(x0)
print(np.argmax(x0))
plt.subplot(3,1,1)
plt.plot(x0)
plt.title('Figure 1: fs')

plt.show()
