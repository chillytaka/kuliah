[sound3,fs1] = audioread("SARON3SL.WAV");
[sound1,fs2] = audioread("SARON1SL.WAV");
[sound5,fs3] = audioread("SARON5SL.WAV");

beat = 50000;

len_3 = length(sound3);
len_5 = length(sound5);
len_1 = length(sound1);

not5 = [sound5; zeros(beat*2-len_5,1);sound5;zeros(beat*6-len_5,1)];
not1 = [zeros(beat,1);sound1;zeros(beat*7-len_1,1)];
not3 = [zeros(beat*2,1);sound3;zeros(beat*6-len_3,1)];

full = not5 + not1 + not3;

sound(full, 48000)


