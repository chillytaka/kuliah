def matrixkofaktor(a, r, c):
    d = []
    for x in range(len(a)):
        if x <> r:
            e = [h for g, h in enumerate(a[x]) if g <> c]
            d.append(e)
    return d


def detmat(a):
    if len(a) == 1:
        return a[0]
    if len(a) == 2:
        return (a[0][0] * a[1][1] - a[0][1]*a[1][0])
    else:
        k = 0.0
        d = a[0]
        s = 1
        for m in xrange(len(a)):
            k = k + d[m] * s * detmat(matrixkofaktor(a, 0, m))
            s = -s
        return k


m = [
    [3, 1, -1],
    [4, 7, -3],
    [2, -2, 5]
]

i = [
    [1, 0, 0],
    [0, 1, 0],
    [0, 0, 1]
]
print detmat(m),detmat(i)