import statistics  # mean, mode, median
import csv  # get csv file content
import matplotlib  # drawing pie diagram
import matplotlib.pyplot as plt  # drawing pie diagram
import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog  # qt5 things
from main import Ui_MainWindow  # ui files from qt designer

hasil = []  # result of every count
data = []


class AppWindow(QMainWindow):

    # initiation
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)  # setup ui and launch

        # button connect
        self.ui.pickFile.clicked.connect(self.pick)
        self.ui.meanB.clicked.connect(self.mean)
        self.ui.medianB.clicked.connect(self.median)
        self.ui.ModB.clicked.connect(self.mod)
        self.ui.pie.clicked.connect(self.draw)
        # end of button connect

        self.show()

    def pick(self):
        global data
        test = QFileDialog.getOpenFileName(
            self, 'Open CSV File', 'probstat/', 'CSV files (*.csv)')
        if test:
            self.ui.file.setText(test[0])
            with open(test[0], newline='')as file:
                reader = csv.DictReader(file)
                data.clear()
                data = list(reader)
            print(test[0])

    def mean(self):
        rata = []
        text = self.ui.textEdit.toPlainText().split(',')
        hasil.clear()
        for a in range(len(text)):
            rata.clear()
            for row in data:
                if (row[text[a]] != ''):
                    rata.append(float(row[text[a]]))
            hasil.append(round(statistics.mean(rata), 3))
        self.ui.result.setText(str(hasil))

    def median(self):
        med = []
        text = self.ui.textEdit.toPlainText().split(',')
        hasil.clear()
        for a in range(len(text)):
            med.clear()
            for row in data:
                if (row[text[a]] != ''):
                    med.append(float(row[text[a]]))
            hasil.append(round(statistics.median(med), 3))
        self.ui.result.setText(str(hasil))

    def mod(self):
        mod = []
        text = self.ui.textEdit.toPlainText().split(',')
        hasil.clear()
        for a in range(len(text)):
            mod.clear()
            for row in data:
                if (row[text[a]] != ''):
                    mod.append(float(row[text[a]]))
            hasil.append(round(statistics.mode(mod), 3))
        self.ui.result.setText(str(hasil))

    def draw(self):
        labels = self.ui.textEdit.toPlainText().split(',')
        fig, ax = plt.subplots()
        ax.pie(hasil, labels=labels,
               autopct='%1.1f%%', startangle=90)
        ax.axis('equal')
        ax.set(title="Result")
        fig.savefig('result.png')
        plt.show()


app = QApplication(sys.argv)
w = AppWindow()
w.show()
sys.exit(app.exec_())
