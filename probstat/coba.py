# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'program.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(739, 507)
        MainWindow.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pickFile = QtWidgets.QPushButton(self.centralwidget)
        self.pickFile.setGeometry(QtCore.QRect(300, 40, 131, 41))
        self.pickFile.setObjectName("pickFile")
        self.file = QtWidgets.QLabel(self.centralwidget)
        self.file.setEnabled(True)
        self.file.setGeometry(QtCore.QRect(100, 90, 541, 21))
        self.file.setObjectName("file")
        self.meanB = QtWidgets.QPushButton(self.centralwidget)
        self.meanB.setGeometry(QtCore.QRect(60, 240, 81, 41))
        self.meanB.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.meanB.setObjectName("meanB")
        self.medianB = QtWidgets.QPushButton(self.centralwidget)
        self.medianB.setGeometry(QtCore.QRect(330, 240, 81, 41))
        self.medianB.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.medianB.setObjectName("medianB")
        self.ModB = QtWidgets.QPushButton(self.centralwidget)
        self.ModB.setGeometry(QtCore.QRect(600, 240, 81, 41))
        self.ModB.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.ModB.setObjectName("ModB")
        self.pie = QtWidgets.QPushButton(self.centralwidget)
        self.pie.setGeometry(QtCore.QRect(270, 390, 201, 61))
        self.pie.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.pie.setObjectName("pie")
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setGeometry(QtCore.QRect(140, 160, 461, 41))
        self.textEdit.setObjectName("textEdit")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(270, 340, 61, 31))
        self.label_2.setObjectName("label_2")
        self.result = QtWidgets.QLabel(self.centralwidget)
        self.result.setGeometry(QtCore.QRect(320, 350, 211, 18))
        self.result.setObjectName("result")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Basic Statistics"))
        self.pickFile.setText(_translate("MainWindow", "Pick File"))
        self.file.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\">Pick a File First</p></body></html>"))
        self.meanB.setText(_translate("MainWindow", "Mean"))
        self.medianB.setText(_translate("MainWindow", "Median"))
        self.ModB.setText(_translate("MainWindow", "Mod"))
        self.pie.setText(_translate("MainWindow", "Draw Pie Diagram"))
        self.textEdit.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.textEdit.setPlaceholderText(_translate("MainWindow", "Select Column, Comma Separated"))
        self.label_2.setText(_translate("MainWindow", "Result :"))
        self.result.setText(_translate("MainWindow", " "))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

