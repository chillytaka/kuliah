#include <iostream>
using namespace std;

int putarKe(int hadap, int putaran){
    int ke = hadap + putaran;
    ke %= 4;
    // 0, 1, 2, 3 berurutan yaitu bawah, kiri, atas, kanan
    return ke;
}

int* maju(int hadap, int putaran, int y, int x){
    int *pindah = new int[3];
    pindah[0] = y;
    pindah[1] = x;
    int ke = putarKe(hadap, putaran);
    if(ke == 0){
        pindah[0] = y+1;
    } else if (ke == 1){
        pindah[1] = x-1;
    } else if (ke == 2){
        pindah[0] = y-1;
    } else if (ke == 3){
        pindah[1] = x+1;
    }
    pindah[2] = ke;
    return pindah;
}

int main(){
    int x, y;
    cin >> y >> x;
    int rute[y][x];
    int i, j;
    for(i=0; i<y; i++){
        for(j=0; j<x; j++){
            cin >> rute[i][j];
        }
    }
    cout << endl;
    // for(i=0; i<y; i++){
    //     for(j=0; j<x; j++){
    //         cout << rute[i][j] << " ";
    //     }
    //     cout << endl;
    // }
    
    int p =0, q= 0;
    //int posisi = rute[q][p];
    //int tujuan = rute[y-1][x-1];
    int langkah = 0;
    int hadap = 3;
    while(q != y-1 || p != x-1){
        int *pindah = maju(hadap, rute[q][p], q, p);
        q = pindah[0];
        p = pindah[1];
        cout << q;
        cout << p << endl;
        hadap = pindah[2];
        langkah++;
    }
    cout << langkah << endl;
}