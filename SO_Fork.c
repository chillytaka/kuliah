#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>

int main(void)
{
    /*
    struct timespec ts1, ts2;
    printf("1 process.... \n");
    clock_gettime(CLOCK_REALTIME, &ts1);

    int hasil;
    for (int i = 0; i != 1000; i++)
    {
        hasil = hasil + i;
    }

    clock_gettime(CLOCK_REALTIME, &ts2);

    printf("%ld.%09ld\n", (long)(ts2.tv_sec - ts1.tv_sec),
           ts2.tv_nsec - ts1.tv_nsec);
    */
    
    pid_t process1;
    struct timespec ts3, ts4;

    process1 = fork();
    printf("2 process... \n");
    clock_gettime(CLOCK_REALTIME, &ts3);

    if (process1 == 0)
    {
        int hasil;
        for (int i = 0; i != 500; i++)
        {
            hasil = hasil + i;
        }
        //clock_gettime(CLOCK_REALTIME, &ts4);
    }
    else
    {
        int hasil;
        for (int i = 501; i != 1000; i++)
        {
            hasil = hasil + i;
        }
    }
    clock_gettime(CLOCK_REALTIME, &ts4);
    printf("%ld.%09ld\n", (long)(ts4.tv_sec - ts3.tv_sec),
           ts4.tv_nsec - ts3.tv_nsec);
    
    
   
    pid_t process4, process2, process3;
    struct timespec ts5, ts6;

    process4 = fork();
    printf("proses 4....");
    clock_gettime(CLOCK_REALTIME, &ts5);
    if (process4 == 0)
    {
        process2 = fork();
        clock_gettime(CLOCK_REALTIME, &ts5);
        if (process2 == 0)
        {
            int hasil;
            for (int i = 0; i != 250; i++)
            {
                hasil = hasil + i;
            }
        }
        else
        {
            int hasil;
            for (int i = 251; i != 500; i++)
            {
                hasil = hasil + i;
            }
        }
    }
    else
    {
        process3 = fork();
        clock_gettime(CLOCK_REALTIME, &ts5);
        if (process3 == 0)
        {
            int hasil;
            for (int i = 501; i != 750; i++)
            {
                hasil = hasil + i;
            }
        }
        else
        {
            int hasil;
            for (int i = 751; i != 1000; i++)
            {
                hasil = hasil + i;
            }
        }
    }
    clock_gettime(CLOCK_REALTIME, &ts6);
    printf("%ld.%09ld\n", (long)(ts6.tv_sec - ts5.tv_sec),
           ts6.tv_nsec - ts5.tv_nsec);
    
    return 0;
}
