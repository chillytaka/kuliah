[y3,Fs3] = audioread("SARON3SL.WAV");
[y5,Fs5] = audioread("SARON5SL.WAV");
[ny6,Fs6] = audioread("SARON6SL.WAV");

ny3 = [y3;zeros(29226,1)];
ny5 = [y5;zeros(69594,1)];

z = 40000;

d3 = length(ny3);
d5 = length(ny5);
d6 = length(ny6);

s1 = [ny5;zeros(z*11-d5,1)];
s2 = [zeros(z*1,1);ny3;zeros(z*10-d3,1)];
s3 = [zeros(z*2,1);ny5;zeros(z*9-d5,1)];
s4 = [zeros(z*3,1);ny3;zeros(z*8-d3,1)];
s5 = [zeros(z*4,1);ny6;zeros(z*7-d6,1)];
s6 = [zeros(z*5,1);ny5;zeros(z*6-d5,1)];
s7 = [zeros(z*6,1);ny6;zeros(z*5-d6,1)];
s8 = [zeros(z*7,1); ny5;zeros(z*4-d5,1)];

full = s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8;

filename = "hasil.wav";
Fs = 48000;
audiowrite(filename,full,Fs);

plot(full);
sound(full);
