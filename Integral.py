import numpy as np
def fi(x): return (x**3)/4 - x**2 + 2


def Secant(tolerance, x1=5, x2=-5, pias=100, atas = 4, akhir = 0):
    if ((fi(x1)**2) <= (tolerance**2)):
        print(x1)
        Trapesium(x1,x2,pias)

    elif ((fi(x2)**2) <= (tolerance**2)):
        print(x2)
        Trapesium(x1,x2,pias)

    else:
        hasil = x2 - ((fi(x2)*(x2-x1))/(fi(x2)-fi(x1)))

        if ((fi(hasil)**2) <= (tolerance**2)):
            print(hasil)
            if (np.round(hasil) > akhir and np.round(hasil) < atas) :
                result = 0
                result = Trapesium(hasil, akhir,pias)
                result = result + Trapesium(atas,hasil,pias) 
                print (result)

        else:
            Secant(tolerance, x2, hasil, pias, atas, akhir)


def Simpson(atas, bawah, pias):
    deltaX = (atas - bawah) / pias
    total = fi(bawah) + fi(atas)
    x = bawah + deltaX
    while (x <= atas):
        if (x % 2.0 == 0):
            total += fi(x) * 2
        else:
            total += fi(x) * 4
        x += deltaX
    result = total * (deltaX / 3)
    print("Simpson : ", result)
    return result

def Simpson38(atas, bawah, pias):
    deltaX = (atas - bawah) / pias
    total = fi(bawah) + fi(atas)
    x = bawah + deltaX
    while (x <= atas):
        if (x % 2.0 == 0):
            total += fi(x) * 2
        else:
            total += fi(x) * 4
        x += deltaX
    result = total * (deltaX*3 / 8)
    print("Simpson : ", result)
    return result

def Trapesium(atas, bawah, pias):
    deltaX = (atas - bawah) / pias
    total = fi(bawah) + fi(atas)
    x = bawah + deltaX
    while (x <= atas):
        total += fi(x) * 2
        x += deltaX
    result = total * (deltaX / 2)
    print("Trapesium : ", result)
    return result


# Simpson(4, 0, 100)
# Trapesium(4, 1, 10000)
Secant(0.000001, 4,1,10000, 4, 1)
